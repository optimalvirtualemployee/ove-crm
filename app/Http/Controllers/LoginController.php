<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;
class LoginController extends Controller
{
    public function home() {
        return view('login');
    }
    function checklogin(Request $request) {
			$this->validate($request, [
				'email'   => 'required|email',
				'password'  => 'required|min:3'
			]);
			$user_data = array(
				'email'  => $request->post('email'),
				'password' => $request->post('password'),
			);;
		if(Auth::attempt($user_data)){

            if(Auth::user()->status=='1'){
                return redirect('/admin/dashboard');
            }
            else{
                return back()->withErrors(['error' => ['User is not active.']]);
            }

		} else {
            return back()->withErrors(['error' => ['Invalid user name or password.']]);
		}
    }

    function successlogin()  {
     	return view('dashbord');
    }

    function logout()  {
	    Auth::logout();
	    return redirect('main');
    }
}
