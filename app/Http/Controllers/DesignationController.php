<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\storage;
use Illuminate\Support\Facades\Auth;
use App\Department;
use Form;
use Session;
use DB;
class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function designation(Request $request) {
     $search = $request->get('search');
     if($search ?? ''){
         $designation = DB::table('tbl_designation')
         ->where('tbl_designation.designation_name','LIKE','%'.$search.'%')
          ->paginate(20);
     }
     else{
      $designation = DB::table('tbl_designation')->paginate(3);   
     }
      return view('admin.designationManagement.designationmanagement',compact('designation'));    
    }
    public function insertdesignation(Request $request){
      $this->validate($request, [
        'department'   => 'required|min:3',
        'department_manager'  => 'required|min:3'
      ]);
      $deptname = $request->department;
      $deptmngname = $request->department_manager;

      $check = DB::table('tbl_department_management')->where('department_name',$deptname)->get();
        if(count($check)>0){
          return redirect('/admin/adddepartment')->with('erromsg','Department Alreday Added');
        }
        $DepartmentDetail = new Department;
        $DepartmentDetail->department_name = $request->department;
        $DepartmentDetail->department_manager = $request->department_manager;
        $DepartmentDetail->addedBy = auth()->user()->id;
        $DepartmentDetail->save();
        return redirect('/admin/departmentmanagement')->with('success_msg','Department Has Been Added Successfully');
    }
    public function enablr_desabledesignation(Request $request){
      $uid = $request->get('uid');
      $sta = $request->get('status');
      if($sta==0){
        $status='1';
        $msg="Reactivated";
      }
      else{
            $status='0';
            $msg="Suspended";
      }
      $user = Department::find($uid);
      $user->status = $status;
      $user->save();
      return response()->json(Session::flash('success_msg','Department  '.$msg.' Department Name '.$user->department_name. ' successfully'));
    }
    public function Edit(Request $request, $id){
        $department = DB::table('tbl_department_management')
        ->where('id',$id)
        ->get();
        return view('admin.departmentmanagement.editdepartment',compact('department'));
    }
    public function updatedesignation(Request $request){
      $this->validate($request, [
        'department'   => 'required|min:3',
        'department_manager'  => 'required|min:3'
      ]);
    	$uid = $request->uid;
    	$dep = Department::find($uid);
    	$dep->department_name = $request->department;
    	$dep->department_manager = $request->department_manager;
    	$dep->addedBy = auth()->user()->id;
    	$dep->save();
    	return redirect('/admin/departmentmanagement')->with('success_msg','Department Has Been Updated Successfully');
    }
    public function viewDetail(Request $request){
      $did = $request->id;
      if($did){
        $dep = Department::where('id', '=', $did)->first();
        $output = '<div class="table-responsive mx-3">
          <!--Table-->
          <table class="table table-hover mb-0">

            <!--Table head-->
            <thead>
              <tr>
                <th class="th-lg">Department Name </th>
                <th class="th-lg">Department Manager</th>
                <th class="th-lg">Status</th>
                <th></th>
              </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>';
            if(!empty($dep)){
        $output .='<tr>
                <td>'.$dep->department_name.'</td>
                <td>'.$dep->department_manager  .'</td>';
                if($dep->status=='1'){
        $output .='<td>Active</td>';
                }
                else{
        $output .='<td>deactivate</td>';
                }
        $output .='</tr>';
              } else { 
        $output .='<tr>
                <td colspan="3">No Record Nound</td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>';
      }
      echo $output;
    }	

}
}
