<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\storage;
use Illuminate\Support\Facades\Auth;
use App\UserManagement;
use Form;
use Session;
use DB;
class TicketManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Ticketlist(Request $request) {
     $search = $request->get('search');
     if($search ?? ''){
         $user = DB::table('tbl_userManagement')
         ->leftjoin('tbl_role','tbl_userManagement.role','=','tbl_role.id')
         ->where('tbl_userManagement.first_name','LIKE','%'.$search.'%')
         ->orwhere('tbl_userManagement.last_name','LIKE','%'.$search.'%')
         ->orwhere('tbl_userManagement.emp_id','LIKE','%'.$search.'%')
         ->orwhere('tbl_userManagement.email_id','LIKE','%'.$search.'%')
         ->orwhere('tbl_userManagement.mobile_no','LIKE','%'.$search.'%')
         ->orwhere('tbl_role.name','LIKE','%'.$search.'%')
         ->select('*','tbl_userManagement.id as id','tbl_userManagement.status as status')
          ->paginate(20);
     }
     else{
      $user = DB::table('tbl_ticketManagement')
      ->select('tbl_ticketManagement.id','tbl_ticketManagement.task_name')
      ->leftjoin('tbl_userManagement','tbl_ticketManagement.allocated_user','=','tbl_userManagement.id')

      ->paginate(3);   
     }
     echo '<pre>';
     print_r($user);
     die();
      return view('admin.ticketManagement.ticketList',compact('user'));    
    }

    public function insertuser(Request $request){

      $this->validate($request, [
        'firstname'  => 'required|min:3',
        'lastname'  => 'required|min:3',
        'empid'  => 'required|min:3',
        'email'  => 'required|email',
        'mobile'  => 'required|numeric|min:10'
      ]);
      $email = $request->email;
      $mobile = $request->mobile;

      $check = DB::table('tbl_userManagement')->where('email_id',$email)->get();
        if(count($check)>0){
          return redirect('/admin/adduser')->with('erromsg','User Alreday Added With This Email Id');
        }
        $UserManagements = new UserManagement;
        $UserManagements->first_name = $request->firstname;
        $UserManagements->last_name = $request->lastname;
        $UserManagements->emp_id = $request->empid;
        $UserManagements->email_id = $request->email;
        $UserManagements->mobile_no = $request->mobile;
        $UserManagements->role = $request->role;
        $UserManagements->addedBy = auth()->user()->id;
        $UserManagements->save();
        return redirect('/admin/usermanagement')->with('success_msg','Department Has Been Added Successfully');
    }
    public function Edit(Request $request, $id){
        $edituser = DB::table('tbl_userManagement')
        ->leftjoin('tbl_role','tbl_userManagement.role','=','tbl_role.id')
        ->select('*','tbl_userManagement.id as id')
        ->where('tbl_userManagement.id',$id)
        ->get();
        return view('admin.userManagement.edituser',compact('edituser'));
    }
    public function updateuser(Request $request){
      $this->validate($request, [
        'firstname'  => 'required|min:3',
        'lastname'  => 'required|min:3',
        'empid'  => 'required|min:3',
        'email'  => 'required|email',
        'mobile'  => 'required|numeric|min:10'
      ]);
      $uid = $request->uid;
      $dep = UserManagement::find($uid);
        $dep->first_name = $request->firstname;
        $dep->last_name = $request->lastname;
        $dep->emp_id = $request->empid;
        $dep->email_id = $request->email;
        $dep->mobile_no = $request->mobile;
        $dep->role = $request->role;
        $dep->addedBy = auth()->user()->id;
        $dep->save();
      return redirect('/admin/usermanagement')->with('success_msg','Department Has Been Updated Successfully');
    }

    public function enablr_desableUser(Request $request){
      $uid = $request->get('uid');
      $sta = $request->get('status');
      if($sta==0){
        $status='1';
        $msg="Reactivated";
      }
      else{
            $status='0';
            $msg="Suspended";
      }
      $user = UserManagement::find($uid);
      $user->status = $status;
      $user->save();
      return response()->json(Session::flash('success_msg','User  '.$msg.' successfully'));
    }


    public function viewDetail(Request $request){
      $did = $request->id;
      if($did){
        $dep = Department::where('id', '=', $did)->first();
        $output = '<div class="table-responsive mx-3">
          <!--Table-->
          <table class="table table-hover mb-0">

            <!--Table head-->
            <thead>
              <tr>
                <th class="th-lg">Department Name </th>
                <th class="th-lg">Department Manager</th>
                <th class="th-lg">Status</th>
                <th></th>
              </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>';
            if(!empty($dep)){
        $output .='<tr>
                <td>'.$dep->department_name.'</td>
                <td>'.$dep->department_manager  .'</td>';
                if($dep->status=='1'){
        $output .='<td>Active</td>';
                }
                else{
        $output .='<td>deactivate</td>';
                }
        $output .='</tr>';
              } else { 
        $output .='<tr>
                <td colspan="3">No Record Nound</td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>';
      }
      echo $output;
    }	

}
}
