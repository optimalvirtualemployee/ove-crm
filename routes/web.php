<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
    });
Route::post('/logins', 'LoginController@checklogin')->name('login');
Route::get('/logout', function(){
   Auth::logout();
   return Redirect::to('login');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware'=>['auth','admin']], function (){

   Route::get('/admin/dashboard', function () {
    return view('admin.dashboard.dashboard');
    });


    //User management
    Route::get('/admin/usermanagement', [
    'as' => 'admin.usermanagementmanagement',
    'uses' => 'UserManagementController@userlist'
    ]);
    //Add user
    Route::get('/admin/adduser', function () {
    return view('admin.userManagement.adduser');
    });
    //insert user
    Route::post('/admin/insertuser', [
    'as' => 'admin.insertuser',
    'uses' => 'UserManagementController@insertuser'
    ]);
    //edit user 
    Route::get('/admin/user/edit/{id}', [
    'as' => 'admin.user.edit',
    'uses' => 'UserManagementController@Edit'
    ]);
    //update user
    Route::post('/admin/updateuser', [
    'as' => 'admin.updateuser',
    'uses' => 'UserManagementController@updateuser'
    ]);
    //Active and deactive Department
    Route::get('/admin/user/enablr_desable/', [
    'as' => 'admin.user.enablr_desable',
    'uses' => 'UserManagementController@enablr_desableUser'
    ]);

    //ticket management
    Route::get('/admin/ticketManagement', [
    'as' => 'admin.ticketManagement',
    'uses' => 'TicketManagementController@Ticketlist'
    ]);












   //project management
    Route::get('/admin/projectmanagement', function () {
    return view('admin.projectmanagement.projectmanagement');
    });
    //department management
    Route::get('/admin/departmentmanagement', [
    'as' => 'admin.departmentmanagement',
    'uses' => 'DepartmentController@department'
    ]);
    //Add Department
    Route::get('/admin/adddepartment', function () {
    return view('admin.departmentmanagement.adddepartment');
    });
    //Insert Department
    Route::post('/admin/insertdepartment', [
    'as' => 'admin.insertdepartment',
    'uses' => 'DepartmentController@insertdepartment'
    ]);
    //update Department
    Route::post('/admin/updatedepartment', [
    'as' => 'admin.updatedepartment',
    'uses' => 'DepartmentController@updatedepartment'
    ]);
    //Active and deactive Department
    Route::get('/admin/department/enablr_desable/', [
    'as' => 'admin.department.enablr_desable',
    'uses' => 'DepartmentController@enablr_desableUser'
    ]);
    //edit department 
    Route::get('/admin/department/edit/{id}', [
    'as' => 'admin.department.edit',
    'uses' => 'DepartmentController@Edit'
    ]);
    //View Department
    Route::get('/admin/department/viewDetail', [
    'as' => 'admin.department.viewDetail',
    'uses' => 'DepartmentController@viewDetail'
    ]);
    //Designation management
    Route::get('/admin/designationmentmanagement', [
    'as' => 'admin.designationmentmanagement',
    'uses' => 'DesignationController@designation'
    ]);
    //Add Designation
    Route::get('/admin/addddesignation', function () {
    return view('admin.designationManagement.adddesignation');
    });
    //Insert Designation
    Route::post('/admin/insertdesignation', [
    'as' => 'admin.insertdesignation',
    'uses' => 'DesignationController@insertdesignation'
    ]);

});