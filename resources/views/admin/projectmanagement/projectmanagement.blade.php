@extends('common.default')
@section('title', 'Dashboard')
@section('content')
 <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

        <!-- Accordion card -->
  <div class="card">

    <!-- Card header -->
    <div class="card-header" role="tab" id="heading79">

      <!--Options-->
      <div class="dropdown float-right">
        add project
      </div>

      <!-- Heading -->
      <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapse79" aria-expanded="true"
        aria-controls="collapse79">
        <h5 class="mt-1 mb-0">
          <span>Project Management</span>
          <!-- <i class="fas fa-angle-down rotate-icon"></i> -->
        </h5>
      </a>

    </div>

    <!-- Card body -->
    <div id="collapse79" class="collapse show" role="tabpanel" aria-labelledby="heading79"
      data-parent="#accordionEx78">
      <div class="card-body">

        <!--Top Table UI-->
        <div class="table-ui p-2 mb-3 mx-3 mb-4">

          <!--Grid row-->
          <div class="row">

         <div class="card-body d-sm-flex justify-content-between">

          <h4 class="mb-2 mb-sm-0 pt-1">
            <!-- <a href="https://mdbootstrap.com/docs/jquery/" target="_blank">Home Page</a> -->
            <!-- <span>/</span> -->
            <span>Project List</span>
          </h4>

          <form class="d-flex justify-content-center">
            <!-- Default input -->
            <input type="search" placeholder="Type your query" aria-label="Search" class="form-control">
            <button class="btn btn-primary btn-sm my-0 p" type="submit">
              <i class="fas fa-search"></i>
            </button>

          </form>

        </div>

          </div>
          <!--Grid row-->

        </div>
        <!--Top Table UI-->

        <!-- Table responsive wrapper -->
        <div class="table-responsive mx-3">
          <!--Table-->
          <table class="table table-hover mb-0">

            <!--Table head-->
            <thead>
              <tr>
                <th>Sl No</th>
                <th class="th-lg">Name </th>
                <th class="th-lg">Condition</th>
                <th class="th-lg">Last edited</th>
                <th></th>
              </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>
              <tr>
                <td>1</td>
                <td>Test subscription</td>
                <td>Scroll % is equal or greater than <strong>80</strong></td>
                <td>12.06.2017</td>
                <td>
                  <a><i class="fas fa-info mx-1" data-toggle="tooltip" data-placement="top"
                      title="Tooltip on top"></i></a>
                  <a><i class="fas fa-pen-square mx-1"></i></a>
                  <a><i class="fas fa-times mx-1"></i></a>
                </td>
              </tr>
              
            </tbody>
            <!--Table body-->
          </table>
          <!--Table-->
        </div>
        <!-- Table responsive wrapper -->

      </div>
    </div>
  </div>
  <!-- Accordion card -->




    </div>
   </main> 
   <script type="text/javascript">
  // Animations initialization
    new WOW().init();
  $(document).ready(function () {
    $('.mdb-select').material_select();
    $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=0;i<4;i++) {
    next=next.next();
    if (!next.length) {
      next=$(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  }
});
});
</script>
  <!--Grid row-->  
  @stop