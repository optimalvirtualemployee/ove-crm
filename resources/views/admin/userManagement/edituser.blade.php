@extends('common.default')
@section('title', 'Add Department')
@section('content')

 <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

<!--Grid row-->
<div class="row wow fadeIn">
  <!--Grid column-->
  <div class="col-md-12 mb-4">
    <!--Card-->
    <div class="card">
      <!--Card content-->
      <div class="card-body">
      <!-- Heading -->
      <h5>Edit User</h5>
      
          <!-- Extended material form grid -->
<form method="POST" action="{{ url('/admin/updateuser') }}">
  @csrf
  <!-- Grid row -->
  <div class="form-row">
    @if($edituser ?? '')
    <!-- Grid column -->
    @foreach($edituser as $dep)
      <div class="col-md-6">
      <!-- Material input -->
      <div class="md-form form-group">
        <input type="text" class="form-control {{ $errors->has('firstname') ? ' is-invalid' : '' }}" 
            name="firstname" value="{{ $dep->first_name}}" id="firstname" placeholder="First Name">
        @error('firstname')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">First Name</label>
      </div>
      <input type="hidden" name="uid" value="{{$dep->id}}">
    </div>
    <div class="col-md-6">
    <div class="md-form form-group">
        <input type="text" class="form-control {{ $errors->has('lastname') ? ' is-invalid' : '' }}" 
            name="lastname" value="{{ $dep->last_name }}" id="lastname" placeholder="Last Name">
        @error('lastname')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">Last Name</label>
      </div>
    </div>
    <div class="col-md-6">
    <div class="md-form form-group">
        <input type="text" class="form-control {{ $errors->has('empid') ? ' is-invalid' : '' }}" 
            name="empid" value="{{ $dep->emp_id }}" id="empid" placeholder="Emp Id">
        @error('empid')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">Emp Id</label>
      </div>
    </div>
    <div class="col-md-6">
    <div class="md-form form-group">
        <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" 
            name="email" value="{{ $dep->email_id }}" id="email" placeholder="Email Id">
        @error('email')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">Email Id</label>
      </div>
    </div>
    <div class="col-md-6">
    <div class="md-form form-group">
        <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" 
            name="mobile" value="{{ $dep->mobile_no }}" id="mobile" placeholder="Mobile Number">
        @error('mobile')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">Mobile Number</label>
      </div>
    </div>

  <div class="col-md-6">
    <div class="md-form form-group">
      <?php $role = DB::table('tbl_role')->where('status','1')->get(); ?>
        <select name="role" id="role" class="form-control">
          @foreach($role as $role)
          <option @if($dep->role==$role->id) selected @endif value="{{$role->id}}">{{$role->name}}</option>
          @endforeach
        </select>
        <!-- <label for="inputEmail4MD">Select Role</label> -->
      </div>
    </div>
    <!-- Grid column -->
    @endforeach
    @endif

    <!-- Grid column -->
  </div>
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog cascading-modal" role="document">
    <!--Content-->

    <!--/.Content-->
  </div>
</div>

<div class="text-center">
  <button type="submit" class="btn btn-default btn-rounded my-3">Submit</button>
</div>

</form>



 
    <!-- Intro Section -->
<!-- Extended material form grid -->
      </div>
    </div>
    <!--/.Card-->
  </div>
  <!--Grid column-->
</div>
<!--Grid row-->  





    </div>
  </main> 
   <script type="text/javascript">
  // Animations initialization
    new WOW().init();
  $(document).ready(function () {
    $('.mdb-select').material_select();
    $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=0;i<4;i++) {
    next=next.next();
    if (!next.length) {
      next=$(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  }
});
});
</script>
  <!--Grid row-->  
  @stop