@extends('common.default')
@section('title', 'Add Department')
@section('content')

 <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

<!--Grid row-->
<div class="row wow fadeIn">
  <!--Grid column-->
  <div class="col-md-12 mb-4">
    <!--Card-->
    <div class="card">
      <!--Card content-->
      <div class="card-body">
      <!-- Heading -->
      <h5>Add User</h5>
              @if ($message = Session::get('erromsg'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>    
    <strong>{{ $message }}</strong>
</div>
@endif
      
          <!-- Extended material form grid -->
<form method="POST" action="{{ url('/admin/insertuser') }}">
  @csrf
  <!-- Grid row -->
  <div class="form-row">

    <div class="col-md-6">
      <!-- Material input -->
      <div class="md-form form-group">
        <input type="text" class="form-control {{ $errors->has('firstname') ? ' is-invalid' : '' }}" 
            name="firstname" value="{{ old('firstname') }}" id="firstname" placeholder="First Name">
        @error('firstname')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">First Name</label>
      </div>
    </div>
    <div class="col-md-6">
    <div class="md-form form-group">
        <input type="text" class="form-control {{ $errors->has('lastname') ? ' is-invalid' : '' }}" 
            name="lastname" value="{{ old('lastname') }}" id="lastname" placeholder="Last Name">
        @error('lastname')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">Last Name</label>
      </div>
    </div>
    <div class="col-md-6">
    <div class="md-form form-group">
        <input type="text" class="form-control {{ $errors->has('empid') ? ' is-invalid' : '' }}" 
            name="empid" value="{{ old('empid') }}" id="empid" placeholder="Emp Id">
        @error('empid')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">Emp Id</label>
      </div>
    </div>
    <div class="col-md-6">
    <div class="md-form form-group">
        <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" 
            name="email" value="{{ old('email') }}" id="email" placeholder="Email Id">
        @error('email')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">Email Id</label>
      </div>
    </div>
    <div class="col-md-6">
    <div class="md-form form-group">
        <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" 
            name="mobile" value="{{ old('mobile') }}" id="mobile" placeholder="Mobile Number">
        @error('mobile')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">Mobile Number</label>
      </div>
    </div>

  <div class="col-md-6">
    <div class="md-form form-group">
      <?php $role = DB::table('tbl_role')->where('status','1')->get(); ?>
        <select name="role" id="role" class="form-control">
          @foreach($role as $role)
          <option value="{{$role->id}}">{{$role->name}}</option>
          @endforeach
        </select>
        <!-- <label for="inputEmail4MD">Select Role</label> -->
      </div>
    </div>
    <!-- Grid column -->
  </div>
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog cascading-modal" role="document">
    <!--Content-->
    <div class="modal-content">

      <!--Header-->
      <div class="modal-header light-blue darken-3 white-text">
        <h4 class="title"><i class="fas fa-pencil-alt"></i> Contact form</h4>
        <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <!--Body-->
      <div class="modal-body mb-0">
        <div class="md-form form-sm">
          <i class="fas fa-envelope prefix active"></i>
          <input type="text" id="form8" class="form-control">
          <label for="form8" class="active">Your name</label>
        </div>

        <div class="md-form form-sm">
          <i class="fas fa-lock prefix active"></i>
          <input type="password" id="form9" class="form-control">
          <label for="form9" class="active">Your email</label>
        </div>

        <div class="md-form form-sm">
          <i class="fas fa-tag prefix"></i>
          <input type="search" id="form-autocomplete-2" class="form-control mdb-autocomplete">
          <button class="mdb-autocomplete-clear">
            <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="https://www.w3.org/2000/svg">
              <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
              <path d="M0 0h24v24H0z" fill="none" />
            </svg>
          </button>
          <label for="form-autocomplete-2" class="active">Subject</label>
        </div>

        <div class="md-form form-sm">
          <i class="fas fa-pencil-alt prefix"></i>
          <textarea type="text" id="form67" class="md-textarea mb-0"></textarea>
          <label for="form67">Your message</label>
        </div>

        <div class="text-center mt-1-half">
          <button class="btn btn-info mb-2 waves-effect waves-light">Send <i class="fas fa-send ml-1"></i></button>
        </div>

      </div>
    </div>
    <!--/.Content-->
  </div>
</div>

<div class="text-center">
  <button type="submit" class="btn btn-default btn-rounded my-3">Submit</button>
</div>

</form>



 
    <!-- Intro Section -->
<!-- Extended material form grid -->
      </div>
    </div>
    <!--/.Card-->
  </div>
  <!--Grid column-->
</div>
<!--Grid row-->  





    </div>
  </main> 
   <script type="text/javascript">
  // Animations initialization
    new WOW().init();
  $(document).ready(function () {
    $('.mdb-select').material_select();
    $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=0;i<4;i++) {
    next=next.next();
    if (!next.length) {
      next=$(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  }
});
});
</script>
  <!--Grid row-->  
  @stop