@extends('common.default')
@section('title', 'Dashboard')
@section('content')
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css">
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>
 <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

      <!-- Accordion card -->
  <div class="card">

    <!-- Card header -->
    <div class="card-header" role="tab" id="heading79">

      <!--Options-->
      <div class="dropdown float-right">
      <a href="{{url('/admin/adduser')}}" class="btn btn-default btn-rounded mb-4 waves-effect waves-light" >
        Add User</a>
      </div>

      <!-- Heading -->
      <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapse79" aria-expanded="true"
        aria-controls="collapse79">
        <h5 class="mt-1 mb-0">
          <span>User Management</span>
          <!-- <i class="fas fa-angle-down rotate-icon"></i> -->
        </h5>
      </a>

    </div>

    <!-- Card body -->
    <div id="collapse79" class="collapse show" role="tabpanel" aria-labelledby="heading79"
      data-parent="#accordionEx78">
      <div class="card-body">

        <!--Top Table UI-->
        <div class="table-ui p-2 mb-3 mx-3 mb-4">
        @if ($message = Session::get('success_msg'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{{ $message }}</strong>
          </div>
        @endif
          <!--Grid row-->
          <div class="row">

         <div class="card-body d-sm-flex justify-content-between">

          <h4 class="mb-2 mb-sm-0 pt-1">
            <!-- <a href="https://mdbootstrap.com/docs/jquery/" target="_blank">Home Page</a> -->
            <!-- <span>/</span> -->
            <span>User List</span>
          </h4>

          <form method="GET" action="{{url('/admin/usermanagement')}}" class="d-flex justify-content-center">
            <!-- Default input -->
            <input type="search" name="search" placeholder="Search" aria-label="Search" class="form-control">
            <button class="btn btn-primary btn-sm my-0 p" type="submit">
              <i class="fas fa-search"></i>
            </button>

          </form>

        </div>

          </div>
          <!--Grid row-->

        </div>
        <!--Top Table UI-->

        <!-- Table responsive wrapper -->
        <div class="table-responsive mx-3">
          <!--Table-->
          <table class="table table-hover mb-0">

            <!--Table head-->
            <thead>
              <tr>
                <th>Sl No</th>
                <th class="th-lg">User Name </th>
                <th class="th-lg">Emp Id </th>
                <th class="th-lg">Email ID</th>
                <th class="th-lg">Mobile Number</th>
                <th class="th-lg">Role</th>
                <th class="th-lg">Last edited</th>
                <th></th>
              </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>
            <?php if(!empty(count($user)) > 0){?>
              @foreach($user as $key=>$data)
              <tr>
                <td>{{ $key+1 }}</td>
                <td>{{$data->first_name}} {{$data->last_name}}</td>
                <td>{{$data->emp_id}}</td>
                <td>{{$data->email_id}}</td>
                <td>{{$data->mobile_no}}</td>
                <td>{{$data->name}}</td>
                <td><?php echo date('d-m-Y', strtotime($data->updated_at));?></td>
                <td>
                  <a><i class="fas fa-info mx-1" data-id="{{$data->id}}" onclick="departmentDetail('{{$data->id}}')"></i></a>
                  <a href="{{ url('/admin/user/edit', $data->id) }}"><i class="fas fa-pen-square mx-1"></i></a>
                  @if($data->status == '1')
                  <a><i class="fas fa-times mx-1" data-id="{{$data->id}}" onclick="status('{{$data->id}}','{{$data->status}}')"></i></a>
                  @else
                  <a><i class="fas fa-check mx-1" data-id="{{$data->id}}" onclick="status('{{$data->id}}','{{$data->status}}')"></i></a>
                  @endif
                </td>
              </tr>
              @endforeach
              <?php } else { ?>
                <tr>
                <td colspan="3">No Record Nound</td>
              </tr>
            <?php } ?>
            </tbody>
            <!--Table body-->
          </table>
          {{$user->withQueryString()->links()}}
          <!--Table-->
        </div>
        <!-- Table responsive wrapper -->

      </div>
    </div>
  </div>
  <!-- Accordion card -->

  <!-- Password Reset Model-->
   <div class="login-id modal fade" id="MyPopup2" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title text-secondary pl-2" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         </div>
        </div>
      </div>
    </div>
  <!-- end Password Reset Model -->


    </div>
   </main> 
   <script type="text/javascript">
  // Animations initialization
    new WOW().init();
  $(document).ready(function () {
    $('.mdb-select').material_select();
    $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=0;i<4;i++) {
    next=next.next();
    if (!next.length) {
      next=$(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  }
});
});

      function status(uid,status){
      var statusType= '';
      if(status == 1){
          statusType = 'Suspend';
      }
      else{
         statusType = 'Activate'; 
      }

          swal({
                text: "Do You want to "+ statusType+ " this Department ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
                   $.ajax({  
                    url: "/admin/user/enablr_desable/",
                    type:'GET',
                    data:{'status':status,'uid':uid}, 
                    success: function(result){
                         location.reload();
                        }
                    });
                  swal("Selected User is "+ statusType, {
                    icon: "success",
                  });
                } 
          });

  }
   
      function departmentDetail(uid){
          var title= 'Department Detail';
          $.ajax({  
              url: "/admin/department/viewDetail",
              type:'GET',
              data:{'id':uid}, 
              success: function(result){
                $("#MyPopup2 .modal-title").html('<p>Department Detail</p>');
                $("#MyPopup2 .modal-body").html(result);
                $("#MyPopup2").modal("show");
              }
          });
      }
</script>
  <!--Grid row-->  
  @stop