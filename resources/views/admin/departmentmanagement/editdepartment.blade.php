@extends('common.default')
@section('title', 'Add Department')
@section('content')

 <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

<!--Grid row-->
<div class="row wow fadeIn">
  <!--Grid column-->
  <div class="col-md-12 mb-4">
    <!--Card-->
    <div class="card">
      <!--Card content-->
      <div class="card-body">
      <!-- Heading -->
      <h5>Edit Department</h5>
      
          <!-- Extended material form grid -->
<form method="POST" action="{{ url('/admin/updatedepartment') }}">
  @csrf
  <!-- Grid row -->
  <div class="form-row">
    @if($department ?? '')
    <!-- Grid column -->
    @foreach($department as $dep)
    <div class="col-md-6">
      <!-- Material input -->
      <div class="md-form form-group">
        <input type="text" class="form-control {{ $errors->has('department') ? ' is-invalid' : '' }}" 
            name="department" value="{{$dep->department_name}}" id="department" placeholder="Department">
        @error('department')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputEmail4MD">Department</label>
      </div>
    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-6">
      <!-- Material input -->
      <div class="md-form form-group">
        <input type="text" class="form-control {{ $errors->has('department_manager') ? ' is-invalid' : '' }}" 
            name="department_manager" value="{{$dep->department_manager}}" id="department_manager" placeholder="Department Management">
        @error('department_manager')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="inputPassword4MD">Department Management</label>
      </div>
    </div>
    <input type="hidden" name="uid" id="uid" value="{{$dep->id}}">
    @endforeach
    @endif
    <!-- Grid column -->
  </div>
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog cascading-modal" role="document">
    <!--Content-->

    <!--/.Content-->
  </div>
</div>

<div class="text-center">
  <button type="submit" class="btn btn-default btn-rounded my-3">Submit</button>
</div>

</form>



 
    <!-- Intro Section -->
<!-- Extended material form grid -->
      </div>
    </div>
    <!--/.Card-->
  </div>
  <!--Grid column-->
</div>
<!--Grid row-->  





    </div>
  </main> 
   <script type="text/javascript">
  // Animations initialization
    new WOW().init();
  $(document).ready(function () {
    $('.mdb-select').material_select();
    $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=0;i<4;i++) {
    next=next.next();
    if (!next.length) {
      next=$(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  }
});
});
</script>
  <!--Grid row-->  
  @stop