<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title')</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="{{url('assets/css/mdb.min.css')}}" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="{{url('assets/css/style.min.css')}}" rel="stylesheet">    

    <style>
      .map-container{
      overflow:hidden;
      padding-bottom:56.25%;
      position:relative;
      height:0;
      }
      .map-container iframe{
      left:0;
      top:0;
      height:100%;
      width:100%;
      position:absolute;
      }
    </style>
</head>
<body class="grey lighten-3">
<!--Main Navigation-->
  <header>
    @include('common.header')
    @include('common.sidebar')
  </header>
  <!--Main Navigation-->
  <!--Main layout-->
    @yield('content')
  <!--Main layout-->
    @include('common/footer')

</body>
</html>