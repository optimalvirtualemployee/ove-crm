<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
  <title>Login</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo e(url('assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo e(url('assets/css/nmdb.min.css')); ?>" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo e(url('assets/css/style.min.css')); ?>" rel="stylesheet">
</head>
<body class="grey lighten-3">
<!--Main layout-->
    <div class="container-fluid mt-5">
      <div class="col-md-12 col-lg-6 col-xl-4 mx-auto mb-4">
        <section class="form-gradient">
            <div class="card" style="margin-top: 150px;">
                <!--Header-->
                <div class="header pt-3 peach-gradient">

                    <div class="row d-flex justify-content-center">
                        <h3 class="white-text mb-3 pt-3 font-weight-bold">Sign in</h3>
                    </div>

                    <div class="row mt-2 mb-3 d-flex justify-content-center">
                        <!--Facebook-->
                        <!-- <a class="icons-sm fb-ic mx-2"><i class="fab fa-facebook-f white-text fa-lg"> </i></a> -->
                        <!--Twitter-->
                        <!-- <a class="icons-sm tw-ic mx-2"><i class="fab fa-twitter white-text fa-lg"> </i></a> -->
                        <!--Google +-->
                        <!-- <a class="icons-sm gplus-ic mx-2"><i class="fab fa-google-plus-g white-text fa-lg"> </i></a> -->
                    </div>
                </div>
                <!--Header-->
                <div class="card-body mx-4 mt-4">
                    <!--Body-->
                    <form method="POST" action="<?php echo e(url('/logins')); ?>">
                        <?php echo csrf_field(); ?>
                           <?php if($errors->any()): ?>
                              <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <div class="alert alert-danger">
                                      <?php echo e($error); ?>

                                  </div>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>
                        <div class="md-form">
                            <input id="email" type="email" class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required autofocus>
                            <?php if($message = Session::get('error')): ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                            <?php endif; ?>
                            <label for="Form-email3">Email Id</label>
                        </div>
                        <div class="md-form">
                            <input id="password" type="password" class="form-control" name="password" required>
                            <?php if($errors->has('password')): ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($errors->first('password')); ?></strong>
                                </span>
                            <?php endif; ?>
                            <label for="Form-pass3">Password</label>
                        </div>
                        <!--Grid row-->
                        <div class="row d-flex mb-4">
                            <!--Grid column-->
                            <div class="text-center mb-3 col-md-12">
                                <button type="submit" class="btn btn-success btn-block btn-rounded z-depth-1 waves-effect waves-light">LOGIN</button>
                            </div>
                            <!--Grid column-->
                        </div>
                    </form>
                        <div class="d-flex justify-content-between">
                          <div class="form-group form-check">
                          <input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                          <label class="form-check-label" for="remember">
                                        <?php echo e(__('Remember Me')); ?>

                          </label>
<!--                             
                              <label class="form-check-label">
                              <input class="form-check-input" type="checkbox"> Remember me
                              </label> -->
                           </div>
                           <div class="forget-password">
                              <a href="#">Forget Password?</a>
                           </div>
                        </div>
                    <!--Grid row-->
                </div>
            </div>
        </section>

      </div>
    </div>
  <!--Main layout-->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo e(url('assets/js/jquery-3.3.1.min.js')); ?>"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo e(url('assets/js/popper.min.js')); ?>"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo e(url('assets/js/bootstrap.min.js')); ?>"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo e(url('assets/js/nmdb.min.js')); ?>"></script>
  <!-- DataTables JS -->

<script type="text/javascript">
  // Animations initialization
    new WOW().init();
  $(document).ready(function () {
    $('.mdb-select').material_select();
    $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=0;i<4;i++) {
    next=next.next();
    if (!next.length) {
      next=$(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  }
});
});
</script>
  
</body>
</html><?php /**PATH /var/www/html/ovecrm/resources/views/auth/login.blade.php ENDPATH**/ ?>